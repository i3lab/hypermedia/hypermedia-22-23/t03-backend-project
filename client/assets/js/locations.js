// Retrieving element from the HTML page.
let container = document.getElementById("card-container");

// Function that create the template for our card.
function createCard(name, city, id) {
    let card = `<div class="card">
    <div class="image-container">
    <img class="dog-img" src="./assets/img/home-image.jpg" />
    </div>
    <span class="dog-name">${name}</span>
    <span class="dog-breed">${city}</span>
    <button onclick=moveTo(${id})>Open description</button>
    </div>`

    return card;
}

async function loadData() {
    const response = await fetch('/locations')
    const body = await response.json()

    for(let location of body) {
        container.innerHTML += createCard(location.name, location.city, location.id);
    }
}

function moveTo(id) {
    window.open('/locationPage.html?id=' + id, "_self")
}

loadData()